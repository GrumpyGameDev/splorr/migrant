let Board = {
    create: function(columns,rows,container,cellWidth,cellHeight,cellInitializer){
        let displayBoard = [];

        while(displayBoard.length<columns){
            let displayColumn = [];
            let displayColumnContainer = new PIXI.Container();
            displayColumnContainer.x = displayBoard.length * cellWidth;
            container.addChild(displayColumnContainer);
            displayBoard.push(displayColumn);
            while(displayColumn.length<rows){
                let displayCell = {};
                let displayCellContainer = new PIXI.Container();
                displayCellContainer.y = displayColumn.length * cellHeight;
                displayColumnContainer.addChild(displayCellContainer);
                displayColumn.push(displayCell);
                cellInitializer(displayCellContainer, displayCell);
            }
        }
        return displayBoard;
    }
};