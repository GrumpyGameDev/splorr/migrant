PIXI.settings.MIPMAP_TEXTURES = PIXI.MIPMAP_MODES.OFF;

let app = new PIXI.Application({
    autoResize: true,
    resolution: devicePixelRatio
});

document.body.appendChild(app.view);

let container = new PIXI.Container();
app.stage.addChild(container);

StateMachine.initialize(container);

let mapContainer = new PIXI.Container();
container.addChild(mapContainer);
let mapBoard = 
    Board.create(
        Constants.mapBoard.cellColumns,
        Constants.mapBoard.cellRows,
        container,
        Constants.mapBoard.cellWidth,
        Constants.mapBoard.cellHeight,
        function(container,cell){
            cell.background = new PIXI.Sprite(TextureManager.getTexture("\u00db"));
            cell.background.width = Constants.mapBoard.cellWidth;
            cell.background.height = Constants.mapBoard.cellHeight;
            cell.background.tint = Colors.onyx;
            cell.setBackground = function(color){
                this.background.tint = color;
            };
            container.addChild(cell.background);


            cell.terrain = new PIXI.Sprite(TextureManager.getTexture("field"));
            cell.terrain.width = Constants.mapBoard.cellWidth;
            cell.terrain.height = Constants.mapBoard.cellHeight;
            cell.terrain.tint = Colors.darkjade;
            cell.setTerrain = function(pattern, color){
                this.terrain.texture = TextureManager.getTexture(pattern);
                this.terrain.tint = color;
            };
            container.addChild(cell.terrain);

            cell.item = new PIXI.Sprite(TextureManager.getTexture("\u0000"));
            cell.item.width = Constants.mapBoard.cellWidth;
            cell.item.height = Constants.mapBoard.cellHeight;
            cell.item.tint = Colors.onyx;
            cell.setItem = function(pattern, color){
                this.item.texture = TextureManager.getTexture(pattern);
                this.item.tint = color;
            };
            container.addChild(cell.item);

            cell.creature = new PIXI.Sprite(TextureManager.getTexture("\u0000"));
            cell.creature.width = Constants.mapBoard.cellWidth;
            cell.creature.height = Constants.mapBoard.cellHeight;
            cell.creature.tint = Colors.onyx;
            cell.setCreature = function(pattern, color){
                this.item.texture = TextureManager.getTexture(pattern);
                this.item.tint = color;
            };
            container.addChild(cell.creature);

            cell.effect = new PIXI.Sprite(TextureManager.getTexture("\u0000"));
            cell.effect.width = Constants.mapBoard.cellWidth;
            cell.effect.height = Constants.mapBoard.cellHeight;
            cell.effect.tint = Colors.onyx;
            cell.setEffect = function(pattern, color){
                this.item.texture = TextureManager.getTexture(pattern);
                this.item.tint = color;
            };
            container.addChild(cell.effect);
        });

mapBoard[7][7].setCreature("tagon", Colors.medium);

window.addEventListener('resize', resize);

function resize() {

    let xscale = window.innerWidth / Constants.displayBoard.boardWidth;
    let yscale = window.innerHeight / Constants.displayBoard.boardHeight;
    let scale = Math.min(xscale,yscale);
    let scaledBoardWidth = scale * Constants.displayBoard.boardWidth;
    let scaledBoardHeight = scale * Constants.displayBoard.boardHeight;

    app.renderer.resize(window.innerWidth, window.innerHeight);

    container.scale.x = scale;
    container.scale.y = scale;
    mapContainer.scale.x = scale * 2;
    mapContainer.scale.y = scale * 2;
    
    container.x = window.innerWidth / 2 - scaledBoardWidth / 2;
    container.y = window.innerHeight / 2 - scaledBoardHeight / 2;
}

resize();