let Constants = {
    displayBoard:{
        cellWidth : 8,
        cellHeight : 8,
        cellColumns : 40,
        cellRows : 30
    },
    mapBoard:{
        cellWidth : 16,
        cellHeight : 16,
        cellColumns : 15,
        cellRows : 15
    }
}
Constants.displayBoard.boardWidth = Constants.displayBoard.cellWidth * Constants.displayBoard.cellColumns;
Constants.displayBoard.boardHeight = Constants.displayBoard.cellHeight * Constants.displayBoard.cellRows;

Constants.mapBoard.boardWidth = Constants.mapBoard.cellWidth * Constants.mapBoard.cellColumns;
Constants.mapBoard.boardHeight = Constants.mapBoard.cellHeight * Constants.mapBoard.cellRows;
