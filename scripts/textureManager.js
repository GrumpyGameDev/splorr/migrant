let TextureManager = {
    textures:{},
    getTexture:function (patternName){
        if(this.textures[patternName]==null)
        {
            let pattern = Patterns[patternName];
            this.textures[patternName] = PIXI.Texture.fromBuffer(pattern.buffer, pattern.width, pattern.height);
        }
        return this.textures[patternName];
    }
};
